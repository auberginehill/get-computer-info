```

   _____      _           _____                            _          _____        __
  / ____|    | |         / ____|                          | |        |_   _|      / _|
 | |  __  ___| |_ ______| |     ___  _ __ ___  _ __  _   _| |_ ___ _ __| |  _ __ | |_ ___
 | | |_ |/ _ \ __|______| |    / _ \| '_ ` _ \| '_ \| | | | __/ _ \ '__| | | '_ \|  _/ _ \
 | |__| |  __/ |_       | |___| (_) | | | | | | |_) | |_| | ||  __/ | _| |_| | | | || (_) |
  \_____|\___|\__|       \_____\___/|_| |_| |_| .__/ \__,_|\__\___|_||_____|_| |_|_| \___/
                                              | |
                                              |_|      
```

# Get-ComputerInfo Wiki

[TOC]

## Screenshot

![Screenshot](https://bitbucket.org/auberginehill/get-computer-info/raw/6af34062297de36b18a7019d534d93d6b3f03d91/Get-ComputerInfo.png)




## Parameters :triangular_ruler:

  - #### Parameter `-Computer`

    with an alias `-ComputerName`. The `-Computer` parameter determines the objects (i.e. the computers) for Get-ComputerInfo. To enter multiple computer names, please separate each individual computer name with a comma. The `-Computer` parameter also takes an array of strings and objects could be piped to this parameter, too. If no value for the `-Computer` parameter is defined in the command launching Get-ComputerInfo, the local machine will be defined as the `-Computer` parameter value.

  - #### Parameter `-Output`

    with an alias `-ReportPath`. Specifies where most of the files are to be saved. The default save location is `$env:temp`, which points to the current temporary file location, which is set in the system. The default `-Output` save location is defined at line 15 with the `$Output` variable. In case the path name includes space characters, please enclose the path name in quotation marks (single or double). For usage, please see the Examples below and for more information about `$env:temp`, please see the Notes section below. Please note that the output folder for the `-GatherNetworkInfo` parameter is hard coded inside the vbs script and cannot be changed with `-Output` parameter.

  - #### Parameter `-File`

    with aliases `-ListOfComputersInATxtFile` and `-List`. The `-File` parameter may be used to define the path to a text file, which contains computer names or IP addresses (one in each line). If the full filename or the directory name includes space characters, please enclose the whole inputted string in quotation marks (single or double).

  - #### Parameter `-SystemInfo`

    If the `-SystemInfo` parameter is added to the command launching Get-ComputerInfo, a `systeminfo.exe /fo LIST` Dos command is eventually launched, which outputs a `system_info.txt` text file.

  - #### Parameter `-Extract`

    with aliases `-ExtractMsInfo32ToAFile`, `-ExtractMsInfo32`, `-MsInfo32ContentsToFile`, `-MsInfo32Report`, `-Expand` and `-Export`. If the `-Extract` parameter is added to the command launching Get-ComputerInfo, the data contained by the System Information (`msinfo32.exe`) program is exported to `ms_info.txt` and `ms_info.nfo` files, and on machines running PowerShell version 5.1 or later the data is also converted to a XML-file. Please note that this step will have a drastical toll on the completion time of this script, because each of the three steps may run for minutes.

  - #### Parameter `-MsInfo32`

    with aliases `-OpenMsInfo32PopUpWindow` and `-Window`. By adding the `-MsInfo32` parameter to the command launching Get-ComputerInfo, the System Information (`msinfo32`) window may be opened.

  - #### Parameter `-GatherNetworkInfo`

    with an alias `-Vbs`. If the `-GatherNetworkInfo` parameter is added to the command launching Get-ComputerInfo, a native `GatherNetworkInfo.vbs` script (which outputs to `$env:temp\Config` folder and doesn't follow the `-Output` parameter) is also eventually executed when Get-ComputerInfo (this script) is run. The vbs script resides in the `%WINDOWS%\system32` directory and amasses an extensive amount of computer related data to the `%TEMP%\Config` directory when run. On most Windows machines the `GatherNetworkInfo.vbs` script has by default a passive scheduled task in the Task Scheduler (i.e. Control Panel → Administrative Tools → Task Scheduler), which for instance can be seen by opening inside the Task Scheduler a Task Scheduler Library → Microsoft → Windows → NetTrace → GatherNetworkInfo tab. The `GatherNetworkInfo.vbs` script will probably run for a few minutes. Please note that for best results it's mandatory to run the GatherNetworkInfo.vbs in an elevated instance (an elevated `cmd`\-prompt or an elevated PowerShell window).

  - #### Parameter `-Cmdlet`

    with aliases `-GetComputerInfoCmdlet` and `-GetComputerInfo`. The parameter `-Cmdlet` will try to launch the native PowerShell `Get-ComputerInfo` cmdlet and output its data to `computer_info.txt` and `computer_info_original.txt` text files. Please note that the inbuilt `Get-ComputerInfo` cmdlet was first introcuded probably in PowerShell v3.1 or in PowerShell v5.1 at the latest. The `Get-Command 'Get-ComputerInfo'` command may search for this cmdlet and `$PSVersionTable.PSVersion` may reveal the PowerShell version.




## Outputs 

:arrow_right:  Opens the generated HTML-file in the default browser. 

  - Displays general computer information (such as Computer Name, Manufacturer, Computer Model, System Type, Domain Role, Product Type, Chassis, PC Type, whether the machine is a laptop or not (based on the chassis information), Model Version, CPU, Video Card, Resolution, Operating System, Architecture, Windows Edition ID, Windows Installation Type, Windows Platform, Type, SP Version, Windows BuildLab Extended, Windows BuildLab, Windows Build Branch, Windows Build Number, Windows Release Id, Current Version, Memory, Video Card Memory, Logical Processors, Cores, Physical Processors, Country Code, OS Language, Video Card Driver Date, BIOS Release Date, OS Install Date, Last BootUp, UpTime, Date, Daylight Bias, Time Offset (Current), Time Offset (Normal), Time (Current), Time (Normal), Daylight In Effect, Time Zone, Connectivity (network adapters), Mobile Broadband, OS Version, PowerShell Version, Video Card Version, BIOS Version, Mother Board Version, Serial Number (BIOS), Serial Number (Mother Board), Serial Number (OS), UUID), and a list of volumes in console.

  - By default writes two files to `$env:temp` or at the location specified with the `-Output` parameter.

  - Default values:

    | Path                           | Type      | Name                 |
    |--------------------------------|-----------|----------------------|
    | `$env:temp\computer_info.html` | HTML-file | `computer_info.html` |
    | `$env:temp\computer_info.csv`  | CSV-file  | `computer_info.csv`  |

  - Optional files with the default `-Output` path (the files are generated, if the corresponding parameters (switches) are added to the command launching Get-ComputerInfo):

    | Path                                   | Parameter (switch)   | Type                              |
    |----------------------------------------|----------------------|-----------------------------------|
    | `$env:temp\system_info.txt`            | `-SystemInfo`        | TXT-file                          |
    | `$env:temp\ms_info.txt`                | `-Extract`           | TXT-file                          |
    | `$env:temp\ms_info.nfo`                | `-Extract`           | NFO-file                          |
    | `$env:temp\ms_info.xml`                | `-Extract`           | XML-file[1]                       |
    | `$env:temp\computer_info.txt`          | `-Cmdlet`            | TXT-file                          |
    | `$env:temp\computer_info_original.txt` | `-Cmdlet`            | TXT-file                          |
    | `$env:temp\Config`                     | `-GatherNetworkInfo` | Folder with files and a subfolder |

    [1] On machines running PowerShell version 5.1 or later




## Notes 

:warning: Please note that all the parameters can be used in one get computer info command.

  - Please note that each of the parameters can be "tab completed" before typing them fully (by pressing the `[tab]` key).

  - Please note that the files (apart from the outputs of the `-GatherNetworkInfo` parameter) are created in a directory, which is end-user settable in each get computer info command with the `-Output` parameter. The default save location is defined with the `$Output` variable (at line 15). The `$env:temp` variable points to the current temp folder. The default value of the `$env:temp` variable is `C:\Users\<username>\AppData\Local\Temp` (i.e. each user account has their own separate temp folder at path `%USERPROFILE%\AppData\Local\Temp`). To see the current temp path, for instance a command...

    `[System.IO.Path]::GetTempPath()`

    ...may be used at the PowerShell prompt window `[PS\>]`.

  - To change the temp folder for instance to `C:\Temp`, please, for example, follow the instructions at [Temporary Files Folder - Change Location in Windows](http://www.eightforums.com/tutorials/23500-temporary-files-folder-change-location-windows.html), which in essence are something along the lines:
    1. Right click Computer icon and select Properties (or select Start → Control Panel → System. On Windows 10 this instance may also be found by right clicking Start and selecting Control Panel → System... or by pressing `[Win-key]` + X and selecting Control Panel → System). On the window with basic information about the computer...
    1. Click on Advanced system settings on the left panel and select Advanced tab on the "System Properties" pop-up window.
    1. Click on the button near the bottom labeled Environment Variables.
    1. In the topmost section, which lists the User variables, both TMP and TEMP may be seen. Each different login account is assigned its own temporary locations. These values can be changed by double clicking a value or by highlighting a value and selecting Edit. The specified path will be used by Windows and many other programs for temporary files. It's advisable to set the same value (a directory path) for both TMP and TEMP.
    1. Any running programs need to be restarted for the new values to take effect. In fact, probably Windows itself needs to be restarted for it to begin using the new values for its own temporary files.




## Examples

:book: To open this code in Windows PowerShell, for instance:

  1. `./Get-ComputerInfo`

    Runs the script. Please notice to insert `./` or `.\` before the script name.

  1. `help ./Get-ComputerInfo -Full`

    Displays the help file.

  1. `./Get-ComputerInfo -Computer dc01, dc02 -Output "E:\chiore" -SystemInfo -Extract -MsInfo32 -Vbs -Cmdlet`

    Runs the script and gets all the available computer related information from the computers `dc01` and `dc02`. Saves most of the results in the "`E:\chiore`" directory (the results of the `GatherNetworkInfo.vbs` are saved to `$env:temp\Config` folder, if the command launching Get-ComputerInfo was run in an elevated PowerShell window). This command will work, because `-Vbs` is an alias of `-GatherNetworkInfo`. Since the path name doesn't contain any space characters, it doesn't need to be enveloped with quotation marks, and furthermore, the word `-Computer` may be left out from this command, too, because the values `dc01` and `dc02` are accepted as computer names due to their position (first).

  1. `New-Item -ItemType File -Path C:\Temp\Get-ComputerInfo.ps1`

    Creates an empty ps1-file to the `C:\Temp` directory. The `New-Item` cmdlet has an inherent `-NoClobber` mode built into it, so that the procedure will halt, if overwriting (replacing the contents) of an existing file is about to happen. Overwriting a file with the `New-Item` cmdlet requires using the `Force`. If the path name and/or the filename includes space characters, please enclose the whole `-Path` parameter value in quotation marks (single or double): `New-Item -ItemType File -Path "C:\Folder Name\Get-ComputerInfo.ps1"`. For more information, please type "`help New-Item -Full`".

  1. `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine`

    This command is altering the Windows PowerShell rights to enable script execution in the default (`LocalMachine`) scope, and defines the conditions under which Windows PowerShell loads configuration files and runs scripts in general. In Windows Vista and later versions of Windows, for running commands that change the execution policy of the `LocalMachine` scope, Windows PowerShell has to be run with elevated rights (Run as Administrator). The default policy of the default (`LocalMachine`) scope is "`Restricted`", and a command "`Set-ExecutionPolicy Restricted`" will "undo" the changes made with the original example above (had the policy not been changed before...). Execution policies for the local computer (`LocalMachine`) and for the current user (`CurrentUser`) are stored in the registry (at for instance the `HKLM:\Software\Policies\Microsoft\Windows\PowerShell\ExecutionPolicy` key), and remain effective until they are changed again. The execution policy for a particular session (`Process`) is stored only in memory, and is discarded when the session is closed.

    |                | PowerShell Execution Policy Parameters                                         |
    |----------------|--------------------------------------------------------------------------------|
    | `Restricted`   | Does not load configuration files or run scripts, but permits individual commands. `Restricted` is the default execution policy. |
    | `AllSigned`    | Scripts can run. Requires that all scripts and configuration files be signed by a trusted publisher, including the scripts that have been written on the local computer. Risks running signed, but malicious, scripts. |
    | `RemoteSigned` | Requires a digital signature from a trusted publisher on scripts and configuration files that are downloaded from the Internet (including e-mail and instant messaging programs). Does not require digital signatures on scripts that have been written on the local computer. Permits running unsigned scripts that are downloaded from the Internet, if the scripts are unblocked by using the `Unblock-File` cmdlet. Risks running unsigned scripts from sources other than the Internet and signed, but malicious, scripts. |
    | `Unrestricted` | Loads all configuration files and runs all scripts. Warns the user before running scripts and configuration files that are downloaded from the Internet. Not only risks, but actually permits, eventually, runningany unsigned scripts from any source. Risks running malicious scripts. |
    | `Bypass`       | Nothing is blocked and there are no warnings or prompts. Not only risks, but actually permits running any unsigned scripts from any source. Risks running malicious scripts. |
    | `Undefined`    | Removes the currently assigned execution policy from the current scope. If the execution policy in all scopes is set to `Undefined`, the effective execution policy is `Restricted`, which is the default execution policy. This parameter will not alter or remove the ("master") execution policy that is set with a Group Policy setting. |
    | **Notes:**     | Please note that the Group Policy setting "`Turn on Script Execution`" overrides the execution policies set in Windows PowerShell in all scopes. To find this ("master") setting, please, for example, open the Local Group Policy Editor (`gpedit.msc`) and navigate to Computer Configuration → Administrative Templates → Windows Components → Windows PowerShell. The Local Group Policy Editor (`gpedit.msc`) is not available in any Home or Starter edition of Windows. |
    |                | For more information, please type "`Get-ExecutionPolicy -List`", "`help Set-ExecutionPolicy -Full`", "`help about_Execution_Policies`" or visit [Set-ExecutionPolicy](https://technet.microsoft.com/en-us/library/hh849812.aspx) or [About Execution Policies](http://go.microsoft.com/fwlink/?LinkID=135170). |




## Contributing

|        |                           |                                                                                                                        |
|:------:|---------------------------|------------------------------------------------------------------------------------------------------------------------|
| :herb: | **Bugs:**                 | Bugs can be reported by creating a new [issue](https://bitbucket.org/auberginehill/get-computer-info/issues/).               |
|        | **Feature Requests:**     | Feature request can be submitted by creating a new [issue](https://bitbucket.org/auberginehill/get-computer-info/issues/).   |
|        | **Editing Source Files:** | New features, fixes and other potential changes can be discussed in further detail by opening a [pull request](https://bitbucket.org/auberginehill/get-computer-info/pull-requests/). |

For further information, please see the [Contributing.md](https://bitbucket.org/auberginehill/get-computer-info/wiki/Contributing.md) page.




## Wiki Features

The wiki itself is actually a git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

```
$ git clone https://auberginehill@bitbucket.org/auberginehill/get-computer-info.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.