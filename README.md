
## Get-ComputerInfo.ps1

|                  |                                                                                                            |
|------------------|------------------------------------------------------------------------------------------------------------|
| **OS:**          | Windows                                                                                                    |
| **Type:**        | A Windows PowerShell script                                                                                |
| **Language:**    | Windows PowerShell                                                                                         |
| **Description:** | Get-ComputerInfo uses Windows Management Instrumentation (WMI) and reads the "`HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion`" registry key to retrieve basic computer information, a list of volumes and partition tables of the computers specified with the `-Computer` parameter (and/or inputted via a text file with the `-File` parameter). The results are displayed on-screen and written to a CSV- and a HTML-file. The default output destination folder `$env:temp`, which points to the current temporary file location, may be changed with the `-Output` parameter.  |
|                  | With five additional parameters (switches) the amount of gathered data may be enlarged: `-SystemInfo` parameter will launch the `systeminfo.exe /fo LIST` Dos command, `-MsInfo32` parameter opens the System Information (`msinfo32`) window, `-Extract` parameter will export the System Information (`msinfo32.exe`) data to a TXT- and a NFO-file (and on machines running PowerShell version 5.1 or later convert the data to a XML-file). The `-GatherNetworkInfo` parameter will launch the native `GatherNetworkInfo.vbs` script (which outputs to `$env:temp\Config` folder and doesn't follow the `-Output` parameter) and `-Cmdlet` parameter will try to launch the native PowerShell `Get-ComputerInfo` cmdlet and output its data to text files. This script is based on clayman2's PowerShell script "[Disk Space](http://powershell.com/cs/media/p/7476.aspx)" (or one of the [archive.org versions](http://web.archive.org/web/20120304222258/http://powershell.com/cs/media/p/7476.aspx)). |
|                  | For further information, please see the [Wiki](https://bitbucket.org/auberginehill/get-computer-info/wiki/).     |
| **Homepage:**    | https://bitbucket.com/auberginehill/get-computer-info                                                          |
|                  | Short URL: https://tinyurl.com/y4ojvazf                                                                    |
| **Version:**     | 2.0                                                                                                        |
| **Downloads:**   | For instance [Get-ComputerInfo.ps1](https://bitbucket.org/auberginehill/get-computer-info/src/master/Get-ComputerInfo.ps1). Or [everything as a .zip-file](https://bitbucket.org/auberginehill/get-computer-info/downloads/). Or `git clone https://auberginehill@bitbucket.org/auberginehill/get-computer-info.git`. Or `git fetch && git checkout master`. |




### Screenshot

![Screenshot](https://bitbucket.org/auberginehill/get-computer-info/raw/6af34062297de36b18a7019d534d93d6b3f03d91/Get-ComputerInfo.png)




### www

|                        |                                                                              |                               |
|:----------------------:|------------------------------------------------------------------------------|-------------------------------|
| :globe_with_meridians: | [Script Homepage](https://github.com/auberginehill/get-computer-info)        |                               |
|                        | [Disk Space](http://powershell.com/cs/media/p/7476.aspx) (or one of the [archive.org versions](http://web.archive.org/web/20120304222258/http://powershell.com/cs/media/p/7476.aspx)) | clayman2 |
|                        | [Validating Computer Lists with PowerShell](https://www.petri.com/validating-computer-lists-with-powershell) | Jeff Hicks |
|                        | [Does anyone know what gatherNetworkInfo.vbs is?](https://answers.microsoft.com/en-us/windows/forum/windows_7-security/does-anyone-know-what-gathernetworkinfovbs-is-its/63a302a6-cf69-4b9a-a3ef-4b2aff1b2514) | Paul-De |
|                        | [How to run exe with/without elevated privileges from PowerShell](http://stackoverflow.com/questions/29266622/how-to-run-exe-with-without-elevated-privileges-from-powershell?rq=1) | alejandro5042 |
|                        | [Powershell show elapsed time](http://stackoverflow.com/questions/10941756/powershell-show-elapsed-time) | Jeff |
|                        | [Powershell - Get-WmiObject and ASSOCIATORS OF Statement](http://learningpcs.blogspot.com/2011/10/powershell-get-wmiobject-and.html) | | 
|                        | [Windows Server 2012 Server Core - Part 5: Tools](https://4sysops.com/archives/windows-server-2012-server-core-part-5-tools/) |  |
|                        | [How to link the output from win32\_diskdrive and win32\_volume](https://social.technet.microsoft.com/Forums/windowsserver/en-US/f82e6f0b-ab97-424b-8e91-508d710e03b1/how-to-link-the-output-from-win32diskdrive-and-win32volume?forum=winserverpowershell) |  |
|                        | [How to use System Information (msinfo32) command-line tool switches](https://support.microsoft.com/en-us/help/300887/how-to-use-system-information-msinfo32-command-line-tool-switches) |  |
|                        | [Windows PowerShell Tip of the Week: More Fun with Dates (and Times)](https://technet.microsoft.com/en-us/library/ff730960.aspx) |  |
|                        | [Systeminfo](https://technet.microsoft.com/en-us/library/bb491007.aspx)      |                               |
|                        | [Msinfo32](https://technet.microsoft.com/en-us/library/bb490937.aspx)        |                               |
|                        | [Win32\_ComputerSystem class](https://msdn.microsoft.com/en-us/library/aa394102(v=vs.85).aspx) |             |
|                        | [Win32\_OperatingSystem class](https://msdn.microsoft.com/en-us/library/aa394239(v=vs.85).aspx)) |           |
|                        | [Win32\_SystemEnclosure class](https://msdn.microsoft.com/en-us/library/aa394474(v=vs.85).aspx) |            |
|                        | [Win32\_VideoController class](https://msdn.microsoft.com/en-us/library/aa394512(v=vs.85).aspx) |            |
|                        | [Win32\_POTSModem class](https://msdn.microsoft.com/en-us/library/aa394360(v=vs.85).aspx) |                  |
|                        | [Win32\_NetworkAdapter class](https://msdn.microsoft.com/en-us/library/aa394216(v=vs.85).aspx) |             |
|                        | [Cscript](https://technet.microsoft.com/en-us/library/ff920171(v=ws.11).aspx) |                              |
|                        | [Stopwatch Class](https://msdn.microsoft.com/en-us/library/system.diagnostics.stopwatch(v=vs.110).aspx) |    |
|                        | [Where-Object](https://msdn.microsoft.com/powershell/reference/5.1/microsoft.powershell.core/Where-Object) | |
|                        | [Start-Job](https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.core/start-job) | |
|                        | [About Jobs](https://msdn.microsoft.com/powershell/reference/5.1/Microsoft.PowerShell.Core/about/about_Jobs) |  |
|                        | [Get-ComputerInfo](https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.management/get-computerinfo) |  |
|                        | [WinRM (Windows Remote Management) Troubleshooting](https://blogs.technet.microsoft.com/jonjor/2009/01/09/winrm-windows-remote-management-troubleshooting/) |  |
|                        | [A Few Good Vista WS-Man (WinRM) Commands](https://blogs.technet.microsoft.com/otto/2007/02/09/a-few-good-vista-ws-man-winrm-commands/) |  |
|                        | [An Introduction to WinRM Basics](https://blogs.technet.microsoft.com/askperf/2010/09/24/an-introduction-to-winrm-basics/) |  |
|                        | [How to Correctly Check if a Process is running and Stop it](http://stackoverflow.com/questions/28481811/how-to-correctly-check-if-a-process-is-running-and-stop-it) |  |
|                        | [Appendix B. Regular Expression Reference](http://powershellcookbook.com/recipe/qAxK/appendix-b-regular-expression-reference) |  |
|                        | [The GatherNetworkInfo.vbs script](http://www.verboon.info/2011/06/the-gathernetworkinfo-vbs-script/) |      |
|                        | [Get-ComputerInfo returns empty values on Windows 10 for most of the properties](https://github.com/PowerShell/PowerShell/issues/3080) |  |
|                        | [The String is the Thing](https://technet.microsoft.com/en-us/library/ee692804.aspx) |                        |
|                        | [Powershellv2 - remove last x characters from a string](http://stackoverflow.com/questions/27175137/powershellv2-remove-last-x-characters-from-a-string#32608908) |  |
|                        | [http://www.figlet.org/](http://www.figlet.org/) and [ASCII Art Text Generator](http://www.network-science.de/ascii/) | ASCII Art |
|                        | [HTML To Markdown Converter](https://digitalconverter.azurewebsites.net/HTML-to-Markdown-converter) |        |
|                        | [Markdown Tables Generator](https://www.tablesgenerator.com/markdown_tables) |                               |
|                        | [HTML table syntax into Markdown](https://jmalarcon.github.io/markdowntables/) |                             |
|                        | [A HTML to Markdown converter written in JavaScript](https://domchristie.github.io/turndown/) | Dom Christie |
|                        | [Paste to Markdown](https://euangoddard.github.io/clipboard2markdown/)       |                               |
|                        | [Convert HTML or anything to Markdown](https://cloudconvert.com/html-to-md)  |                               |
